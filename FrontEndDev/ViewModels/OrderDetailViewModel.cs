﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FrontEndDev.ViewModels
{
    public class OrderDetailViewModel
    {
        public int OrderID { get; set; }
        public int ProductID { get; set; }

        [Required]
        public decimal UnitPrice { get; set; }

        [Required]
        [Min(1)]
        public short Quantity { get; set; }

        [Required]
        [Range(0, 1)]
        [UIHint("Float")]
        public float Discount { get; set; }
    }
}