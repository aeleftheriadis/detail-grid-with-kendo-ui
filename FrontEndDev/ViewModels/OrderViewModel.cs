﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndDev.ViewModels
{
    public class OrderViewModel
    {
        public int OrderID { get; set; }
        public string CustomerID { get; set; }
        public int? EmployeeID { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime? OrderDate { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime? RequiredDate { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime? ShippedDate { get; set; }

        public int? ShipVia { get; set; }

        [Required]
        public decimal? Freight { get; set; }

        [Required]
        public string ShipName { get; set; }

        [Required]
        public string ShipAddress { get; set; }

        [Required]
        public string ShipCity { get; set; }

        public string ShipRegion { get; set; }

        [Required]
        public string ShipPostalCode { get; set; }

        [Required]
        public string ShipCountry { get; set; }
    }
}
