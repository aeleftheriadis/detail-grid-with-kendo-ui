﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FrontEndDev.Models;

namespace FrontEndDev.ViewModels
{
    public class OrdersViewModel
    {
        public IEnumerable<Customer> Customers { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
        public IEnumerable<Shipper> Shippers { get; set; }
        public IEnumerable<Product> Products { get; set; }
    }
}