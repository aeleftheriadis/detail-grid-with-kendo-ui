
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace FrontEndDev.Models
{ 
    public class CustomerRepository : ICustomerRepository
    {
        NorthwindEntities context = new NorthwindEntities();

        public IQueryable<Customer> All
        {
            get { return context.Customers; }
        }

        public IQueryable<Customer> AllIncluding(params Expression<Func<Customer, object>>[] includeProperties)
        {
            IQueryable<Customer> query = context.Customers;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Customer Find(string id)
        {
            return context.Customers.Single(x => x.CustomerID == id);
        }

        public void InsertOrUpdate(Customer customer)
        {
            if (customer.CustomerID == default(string)) {
                // New entity
                context.Customers.Add(customer);
            } else {
                // Existing entity
                context.Customers.Attach(customer);
                ((IObjectContextAdapter)context).ObjectContext.ObjectStateManager.ChangeObjectState( customer, EntityState.Modified );
            }
        }

        public void Delete(string id)
        {
            var customer = context.Customers.Single(x => x.CustomerID == id);
            context.Customers.Remove( customer );
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }

    public interface ICustomerRepository
    {
        IQueryable<Customer> All { get; }
        IQueryable<Customer> AllIncluding(params Expression<Func<Customer, object>>[] includeProperties);
        Customer Find(string id);
        void InsertOrUpdate(Customer customer);
        void Delete(string id);
        void Save();
    }
}