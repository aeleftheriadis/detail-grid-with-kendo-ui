
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace FrontEndDev.Models
{ 
    public class CategoryRepository : ICategoryRepository
    {
        NorthwindEntities context = new NorthwindEntities();

        public IQueryable<Category> All
        {
            get { return context.Categories; }
        }

        public IQueryable<Category> AllIncluding(params Expression<Func<Category, object>>[] includeProperties)
        {
            IQueryable<Category> query = context.Categories;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Category Find(int id)
        {
            return context.Categories.Single(x => x.CategoryID == id);
        }

        public void InsertOrUpdate(Category category)
        {
            if (category.CategoryID == default(int)) {
                // New entity
                context.Categories.Add(category);
            } else {
                // Existing entity
                context.Categories.Attach(category);
                ((IObjectContextAdapter)context).ObjectContext.ObjectStateManager.ChangeObjectState( category, EntityState.Modified );
            }
        }

        public void Delete(int id)
        {
            var category = context.Categories.Single(x => x.CategoryID == id);
            context.Categories.Remove( category );
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }

    public interface ICategoryRepository
    {
        IQueryable<Category> All { get; }
        IQueryable<Category> AllIncluding(params Expression<Func<Category, object>>[] includeProperties);
        Category Find(int id);
        void InsertOrUpdate(Category category);
        void Delete(int id);
        void Save();
    }
}