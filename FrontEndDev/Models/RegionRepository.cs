
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace FrontEndDev.Models
{ 
    public class RegionRepository : IRegionRepository
    {
        NorthwindEntities context = new NorthwindEntities();

        public IQueryable<Region> All
        {
            get { return context.Regions; }
        }

        public IQueryable<Region> AllIncluding(params Expression<Func<Region, object>>[] includeProperties)
        {
            IQueryable<Region> query = context.Regions;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Region Find(int id)
        {
            return context.Regions.Single(x => x.RegionID == id);
        }

        public void InsertOrUpdate(Region region)
        {
            if (region.RegionID == default(int)) {
                // New entity
                context.Regions.Add(region);
            } else {
                // Existing entity
                context.Regions.Attach(region);
                ((IObjectContextAdapter)context).ObjectContext.ObjectStateManager.ChangeObjectState( region, EntityState.Modified );
            }
        }

        public void Delete(int id)
        {
            var region = context.Regions.Single(x => x.RegionID == id);
            context.Regions.Remove( region );
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }

    public interface IRegionRepository
    {
        IQueryable<Region> All { get; }
        IQueryable<Region> AllIncluding(params Expression<Func<Region, object>>[] includeProperties);
        Region Find(int id);
        void InsertOrUpdate(Region region);
        void Delete(int id);
        void Save();
    }
}