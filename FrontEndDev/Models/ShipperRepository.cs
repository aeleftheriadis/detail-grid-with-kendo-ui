
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace FrontEndDev.Models
{ 
    public class ShipperRepository : IShipperRepository
    {
        NorthwindEntities context = new NorthwindEntities();

        public IQueryable<Shipper> All
        {
            get { return context.Shippers; }
        }

        public IQueryable<Shipper> AllIncluding(params Expression<Func<Shipper, object>>[] includeProperties)
        {
            IQueryable<Shipper> query = context.Shippers;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Shipper Find(int id)
        {
            return context.Shippers.Single(x => x.ShipperID == id);
        }

        public void InsertOrUpdate(Shipper shipper)
        {
            if (shipper.ShipperID == default(int)) {
                // New entity
                context.Shippers.Add(shipper);
            } else {
                // Existing entity
                context.Shippers.Attach(shipper);
                ((IObjectContextAdapter)context).ObjectContext.ObjectStateManager.ChangeObjectState( shipper, EntityState.Modified );
            }
        }

        public void Delete(int id)
        {
            var shipper = context.Shippers.Single(x => x.ShipperID == id);
            context.Shippers.Remove( shipper );
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }

    public interface IShipperRepository
    {
        IQueryable<Shipper> All { get; }
        IQueryable<Shipper> AllIncluding(params Expression<Func<Shipper, object>>[] includeProperties);
        Shipper Find(int id);
        void InsertOrUpdate(Shipper shipper);
        void Delete(int id);
        void Save();
    }
}