
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace FrontEndDev.Models
{ 
    public class CustomerDemographicRepository : ICustomerDemographicRepository
    {
        NorthwindEntities context = new NorthwindEntities();

        public IQueryable<CustomerDemographic> All
        {
            get { return context.CustomerDemographics; }
        }

        public IQueryable<CustomerDemographic> AllIncluding(params Expression<Func<CustomerDemographic, object>>[] includeProperties)
        {
            IQueryable<CustomerDemographic> query = context.CustomerDemographics;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public CustomerDemographic Find(string id)
        {
            return context.CustomerDemographics.Single(x => x.CustomerTypeID == id);
        }

        public void InsertOrUpdate(CustomerDemographic customerdemographic)
        {
            if (customerdemographic.CustomerTypeID == default(string)) {
                // New entity
                context.CustomerDemographics.Add(customerdemographic);
            } else {
                // Existing entity
                context.CustomerDemographics.Attach(customerdemographic);
                ((IObjectContextAdapter)context).ObjectContext.ObjectStateManager.ChangeObjectState( customerdemographic, EntityState.Modified );
            }
        }

        public void Delete(string id)
        {
            var customerdemographic = context.CustomerDemographics.Single(x => x.CustomerTypeID == id);
            context.CustomerDemographics.Remove( customerdemographic );
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }

    public interface ICustomerDemographicRepository
    {
        IQueryable<CustomerDemographic> All { get; }
        IQueryable<CustomerDemographic> AllIncluding(params Expression<Func<CustomerDemographic, object>>[] includeProperties);
        CustomerDemographic Find(string id);
        void InsertOrUpdate(CustomerDemographic customerdemographic);
        void Delete(string id);
        void Save();
    }
}