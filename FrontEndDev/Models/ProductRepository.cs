
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace FrontEndDev.Models
{ 
    public class ProductRepository : IProductRepository
    {
        NorthwindEntities context = new NorthwindEntities();

        public IQueryable<Product> All
        {
            get { return context.Products; }
        }

        public IQueryable<Product> AllIncluding(params Expression<Func<Product, object>>[] includeProperties)
        {
            IQueryable<Product> query = context.Products;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Product Find(int id)
        {
            return context.Products.Single(x => x.ProductID == id);
        }

        public void InsertOrUpdate(Product product)
        {
            if (product.ProductID == default(int)) {
                // New entity
                context.Products.Add(product);
            } else {
                // Existing entity
                context.Products.Attach(product);
                ((IObjectContextAdapter)context).ObjectContext.ObjectStateManager.ChangeObjectState( product, EntityState.Modified );
            }
        }

        public void Delete(int id)
        {
            var product = context.Products.Single(x => x.ProductID == id);
            context.Products.Remove( product );
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }

    public interface IProductRepository
    {
        IQueryable<Product> All { get; }
        IQueryable<Product> AllIncluding(params Expression<Func<Product, object>>[] includeProperties);
        Product Find(int id);
        void InsertOrUpdate(Product product);
        void Delete(int id);
        void Save();
    }
}