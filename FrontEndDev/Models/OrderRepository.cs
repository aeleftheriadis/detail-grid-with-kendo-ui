
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace FrontEndDev.Models
{ 
    public class OrderRepository : IOrderRepository
    {
        NorthwindEntities context = new NorthwindEntities();

        public IQueryable<Order> All
        {
            get { return context.Orders; }
        }

        public IQueryable<Order> AllIncluding(params Expression<Func<Order, object>>[] includeProperties)
        {
            IQueryable<Order> query = context.Orders;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Order Find(int id)
        {
            return context.Orders.Single(x => x.OrderID == id);
        }

        public void InsertOrUpdate(Order order)
        {
            if (order.OrderID == default(int)) {
                // New entity
                context.Orders.Add(order);
            } else {
                // Existing entity
                context.Orders.Attach(order);
                ((IObjectContextAdapter)context).ObjectContext.ObjectStateManager.ChangeObjectState( order, EntityState.Modified );
            }
        }

        public void Delete(int id)
        {
            var order = context.Orders.Single(x => x.OrderID == id);
            order.Order_Details.Clear();
            context.Orders.Remove( order );
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }

    public interface IOrderRepository
    {
        IQueryable<Order> All { get; }
        IQueryable<Order> AllIncluding(params Expression<Func<Order, object>>[] includeProperties);
        Order Find(int id);
        void InsertOrUpdate(Order order);
        void Delete(int id);
        void Save();
    }
}