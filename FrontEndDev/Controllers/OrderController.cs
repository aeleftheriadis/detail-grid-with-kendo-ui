﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FrontEndDev.Models;
using FrontEndDev.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace FrontEndDev.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly IOrderRepository orderRepository = new OrderRepository();
        private readonly IShipperRepository shippersRepository = new ShipperRepository();
        private readonly ICustomerRepository customersRepository = new CustomerRepository();
        private readonly IEmployeeRepository employeesRepository = new EmployeeRepository();
        private readonly IProductRepository productsRepository = new ProductRepository();

        public ActionResult Index()
        {
            var viewModel = new OrdersViewModel();
            viewModel.Shippers = shippersRepository.All;
            viewModel.Customers = customersRepository.All;
            viewModel.Employees = employeesRepository.All;
            viewModel.Products = productsRepository.All;
            return View(viewModel);
        }


        public ActionResult Order_Read([DataSourceRequest] DataSourceRequest request)
        {

            DataSourceResult result = orderRepository.All.ToDataSourceResult(request, order => new 
                {
                    OrderID = order.OrderID,
                    CustomerID = order.CustomerID,
                    OrderDate = order.OrderDate,
                    RequiredDate = order.RequiredDate,
                    ShippedDate = order.ShippedDate,
                    Freight = order.Freight,
                    ShipName = order.ShipName,
                    ShipAddress = order.ShipAddress,
                    ShipCity = order.ShipCity,
                    ShipRegion = order.ShipRegion,
                    ShipPostalCode = order.ShipPostalCode,
                    ShipCountry = order.ShipCountry,
                    EmployeeID = order.EmployeeID,
                    ShipVia = order.ShipVia
                }
            );

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Order_Create([DataSourceRequest] DataSourceRequest request, Order order)
        {

            if (order != null && ModelState.IsValid)
            {
                orderRepository.InsertOrUpdate(order);
                orderRepository.Save();
            }

            return Json(new[] { order }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Order_Update([DataSourceRequest] DataSourceRequest request, Order order)
        {
            if (order != null && ModelState.IsValid)
            {
                Order savedOrder = orderRepository.Find(order.OrderID);
                if (savedOrder != null)
                {
                    orderRepository.InsertOrUpdate(savedOrder);
                    orderRepository.Save();
                }
            }

            return Json(new[] { order }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Order_Destroy([DataSourceRequest] DataSourceRequest request, Order order)
        {
            if (order != null)
            {
                orderRepository.Delete(order.OrderID);
                orderRepository.Save();
            }

            return Json(new[] { order }.ToDataSourceResult(request, ModelState));
        }



        public ActionResult Order_Details_Read(int orderDetailOrderID, [DataSourceRequest] DataSourceRequest request)
        {
            if(orderDetailOrderID == 0)
            {
                return Json(new { });
            }

            Order order = orderRepository.Find(orderDetailOrderID);
            if (order != null)
            {
                DataSourceResult result = order.Order_Details.ToDataSourceResult(request, orderDetail => new
                {
                    OrderID = orderDetail.OrderID,
                    ProductID = orderDetail.ProductID,
                    Quantity = orderDetail.Quantity,
                    UnitPrice = orderDetail.UnitPrice,
                    Discount = orderDetail.Discount
                });
                return Json(result);
            }
            else
            {
                return Json(new { });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Order_Details_Create(int orderDetailOrderID, [DataSourceRequest] DataSourceRequest request, Order_Detail orderDetail)
        {

            Order order = orderRepository.Find(orderDetailOrderID);

            Order_Detail selectedDetailProduct = order.Order_Details.FirstOrDefault(detail => detail.ProductID == orderDetail.ProductID);

            if (selectedDetailProduct != null)
            {
                ModelState.AddModelError(key: "ProductID", errorMessage: "This product is already added in this Order");
            }

            if (ModelState.IsValid)
            {
            
                order.Order_Details.Add(orderDetail);
                orderRepository.Save();
            }

            return Json(new[] { orderDetail }.ToDataSourceResult(request, ModelState));

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Order_Details_Update(int orderDetailOrderID, [DataSourceRequest] DataSourceRequest request, Order_Detail orderDetail)
        {
            Order order = orderRepository.Find(orderDetailOrderID);
            Order_Detail selectedDetail = order.Order_Details.FirstOrDefault(detail => detail.ProductID == orderDetail.ProductID);

            if (selectedDetail == null)
            {
                ModelState.AddModelError(key: "ProductID", errorMessage: "This order detail is empty");
            }

            if (ModelState.IsValid)
            {            
                order.Order_Details.Add(selectedDetail);
                orderRepository.Save();
            }

            return Json(new[] { orderDetail }.ToDataSourceResult(request, ModelState));

        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Order_Details_Destroy(int orderDetailOrderID, [DataSourceRequest] DataSourceRequest request, Order_Detail orderDetail)
        {
            Order order = orderRepository.Find(orderDetailOrderID);
            Order_Detail selectedDetail = order.Order_Details.FirstOrDefault(detail => detail.ProductID == orderDetail.ProductID);

            if (selectedDetail != null)
            {
                order.Order_Details.Remove(orderDetail);
                orderRepository.Save();
            }

            return Json(new[] { orderDetail }.ToDataSourceResult(request, ModelState));

        }
    }
}